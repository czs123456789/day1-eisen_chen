Objective: I learned the concept map, which helps with memory. And the team  collaborated to complete a concept map of what a computer is. And I learned the concept about full stack.
Reflective:  Fulfilled.
Interpretive: Concept maps are graphical tools for organizing and representing knowledge.
Decisional:  Concept maps are helpful for learning the full stack. It can help connect the relationships between various technologies. Especially Java, a language with many methods.
